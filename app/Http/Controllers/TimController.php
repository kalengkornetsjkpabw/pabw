<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tim;
class TimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
      $users = tim::all()->toArray();
      $tims = tim::all()->toArray();
      return view('buattim',compact('users','tims'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('tim.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'nama'           => 'required',
        // 'lokasi'    =>  'required',
        // 'provinsi ' => 'required',
        'kabupaten' => ' required',
        'kecamatan' => 'required',
        'pos' => 'required'
      ]);
      $tim = new tim([
           'nama'    =>  $request->get('nama'),
           // 'lokasi'     =>  $request->get('lokasi'),
           'provinsi'     =>  $request->get('provinsi'),
           'kabupaten'     =>  $request->get('kabupaten'),
           'kecamatan'     =>  $request->get('kecamatan'),
           'pos'     =>  $request->get('pos'),
       ]);
       $tim->save();
       return redirect('bentuktim');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = user::find($id);
      return view('buattim', compact('user', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'id_tim'           => 'required',
]);
  $tim = tim::find($id);
  $tim ->nama = $request->get('nama');
  // $tim->lokasi = $request->get('lokasi');
  $tim->provinsi = $request->get('provinsi');
  $tim->kabupaten = $request->get('kabupaten');
  $tim->kecamatan = $request->get('kecamatan');
  $tim->pos = $request->get('pos');
  $tim->save();
  return redirect()->route('bentuktim')->with('success', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
