
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<!-- <link rel="icon" type="image/png" href="img/icons/favicon.ico"/> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap2/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
		<div class="container">
			<a class="navbar-brand js-scroll-trigger" href="/firstpage">SIGAP</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="/firstpage">Home</a>
					</li>
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  Login<span class="caret"></span>
                	<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    	<a class="dropdown-item" href="/login">
                        Login Tenaga Kesehatan
                      </a>
                      <a class="dropdown-item" href="/login2">
                        Login Staf Pengelola
                      </a>
            </a>
				</ul>
			</div>
		</div>
	</nav>

<div class="limiter">
    <div class="container-login100" style="background-image: url('img/bg-03.jpg');">
        <div class="wrap-login100 p-t-30 p-b-50">
              <span class="login100-form-title p-b-41">
                Login Koordinator
              </span>


                <!-- <div class="login100-form validate-form p-b-33 p-t-5"> -->

          <form class="login100-form validate-form p-b-33 p-t-5" action="{{ url('/loginPost2') }}" method="post" >
              {{ csrf_field() }}
                            <div class="wrap-input100 validate-input">

                                <input type="email" class="input100" name="email" value="{{ old('email') }}"  placeholder="Email">
                                <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                            </div>


                        <div class="wrap-input100 validate-input">

                                <input type="password" class="input100" name="password" placeholder="Password">
                                <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                        </div>


                        <div class="container-login100-form-btn m-t-32">
                                <button type="submit" class="login100-form-btn">
                                Login
                                </button>

                            </div>
                          <!-- </div> -->
                    </form>

            </div>
        </div>
    </div>
</div>

<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap2/js/popper.js"></script>
<script src="vendor/bootstrap2/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
</body>
</html>
