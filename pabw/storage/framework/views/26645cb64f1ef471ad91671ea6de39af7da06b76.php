
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">

  <title>Sigap</title>
  <!-- table css -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajak/libs/
  jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app2.css')); ?>">

  </script>
  <link href="css/bootstrap2.min.css" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="css/creative.min.css" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
</head>

<body>

  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/admin/adminpage"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/admin/adminpage"><font size="2">Home</font></a>
                  </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <font size="2" > Staf Pengelola</font>
                       <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              <?php echo e(__('Logout')); ?>

                          </a>

                          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                              <?php echo csrf_field(); ?>
                          </form>
                      </div>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>

        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Tambah Tenaga Kesehatan</h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
                  </div>
              </div>
            </div>
          </div>
        </section>
      </div>
</div>
<br><br>
<br><br>
<br><br>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" >


                <div class="card-body" style="  height: 600px;  width: 600px;">
                    <form method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Nama')); ?></label>

                            <div class="col-lg-8">
                                <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>" required autofocus>

                                <?php if($errors->has('name')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Alamat Email')); ?></label>

                            <div class="col-lg-8">
                                <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required>

                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <br>
                        <!-- <div class="form-group row">
                            <label for="tinggal" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Tempat Tinggal')); ?></label>

                            <div class="col-lg-8">
                                <input id="tinggal" type="text" class="form-control<?php echo e($errors->has('tinggal') ? ' is-invalid' : ''); ?>" name="tinggal" value="<?php echo e(old('tinggal')); ?>" required>

                                <?php if($errors->has('tinggal')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('tinggal')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> -->

                        <div class="form-group row">
                          <label for="provinsi" class="col-md-4 col-form-label text-md-right">Provinsi</label>
                          <div class="col-lg-8">
                          <input list="provinsi" name="provinsi" class="form-control">
                          <datalist id="provinsi" >
                                  <?php

                                  $provinsis=DB::table('provinces')->get();

                                  ?>

                                  <?php $__currentLoopData = $provinsis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <option value="<?php echo e($provinsi->name); ?>">
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </datalist>
                          </div>
                        </div>
                        <br>
                        <div class="form-group row">
                          <label for="kabupaten" class="col-md-4 col-form-label text-md-right">Kabupaten/Kota</label>
                          <div class="col-lg-8">
                          <input list="kabupaten" name="kabupaten" class="form-control">
                          <datalist id="kabupaten" >
                                  <?php

                                  $regencies=DB::table('regencies')->get();

                                  ?>

                                  <?php $__currentLoopData = $regencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kabupaten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <option value="<?php echo e($kabupaten->name); ?>">
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </datalist>
                          </div>
                        </div>
                        <br>
                        <div class="form-group row">
                          <label for="kecamatan" class="col-md-4 col-form-label text-md-right">Kecamatan</label>
                          <div class="col-lg-8">
                          <input list="kecamatan" name="kecamatan" class="form-control">
                          <datalist id="kecamatan" >
                                  <?php

                                  $villages=DB::table('villages')->get();

                                  ?>

                                  <?php $__currentLoopData = $villages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                           <option value="<?php echo e($kecamatan->name); ?>">
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </datalist>
                          </div>
                        </div>
                        <br>
                                      <div class="form-group row">
                                          <label for="pos" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Kode Pos')); ?></label>

                                          <div class="col-lg-8">
                                              <input id="pos" type="pos" class="form-control<?php echo e($errors->has('pos') ? ' is-invalid' : ''); ?>" name="pos" value="<?php echo e(old('pos')); ?>" required>

                                              <?php if($errors->has('pos')): ?>
                                                  <span class="invalid-feedback" role="alert">
                                                      <strong><?php echo e($errors->first('pos')); ?></strong>
                                                  </span>
                                              <?php endif; ?>
                                          </div>
                                      </div>

                        <br>
                        <div class="form-group row">
                          <label for="gender" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Jenis Kelamin')); ?></label>

                          <div class="form-check form-check-inline">
                            <input id="gender" type="radio" class="form-check-input<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>" name="gender" value="Laki-laki" required>
                            <label for="gender" class="form-check-label"  for="inlineRadio2">Laki-laki</label>
                          </div>
                          <div class="form-check form-check-inline">
                            <input id="gender"  class="form-check-input<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>" type="radio" name="gender" value="Perempuan" required>
                            <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                          </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label for="keahlian" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Keahlian')); ?></label>

                            <div class="col-lg-8">
                                <input id="keahlian" type="text" class="form-control<?php echo e($errors->has('keahlian') ? ' is-invalid' : ''); ?>" name="keahlian" value="<?php echo e(old('keahlian')); ?>" required>

                                <?php if($errors->has('keahlian')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('keahlian')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <br>

                        <div class="form-group row" style="display: none;">
                            <label for="status" class="col-md-4 col-form-label text-md-right"><?php echo e(__('status')); ?></label>

                            <div class="col-lg-8"  >
                                <input id="status" type="text" class="form-control<?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>" name="status" value="tersedia" required>

                                <?php if($errors->has('status')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('status')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Password')); ?></label>

                            <div class="col-lg-8">
                                <input  id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><?php echo e(__('Confirm Password')); ?></label>

                            <div class="col-lg-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0" >
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success" style="  height: 30px;  width: 90px;">
                                    <?php echo e(__('Tambah')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<br><br><br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->





        <!-- Main js -->
    		<script src="js/jquery-1.11.1.min.js"></script>
    		<!-- Modernizr js -->
    		<script src="js/modernizr-2.8.1.min.js"></script>
    		<!-- Bootstrap js -->
    		<script src="js/bootstrap.min.js"></script>
    		<!-- owl.carousel js -->
    		<script src="js/owl.carousel.min.js"></script>
    		<!-- countTo js -->
    		<script src="js/jquery.countTo.js"></script>
    		<!-- stellar js -->
    		<script src="js/stellar.js"></script>
    		<!-- YTPlayer js -->
    		<script src="js/jquery.mb.YTPlayer.js"></script>
    		<script src="js/sidebarEffects.js"></script>
    		<script src="js/classie.js"></script>
    		<!-- smoothscroll js -->
    		<script src="js/smoothscroll.min.js"></script>
    		<!-- viewport js -->
    		<script src="js/jquery.inview.min.js"></script>
    		<!-- Scripts js -->
    		<script src="js/scripts.js"></script>

    		<script src="vendor/jquery/jquery.min.js"></script>
    		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    		<!-- Plugin JavaScript -->
    		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    		<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    		<!-- Custom scripts for this template -->
    		<script src="js/creative.min.js"></script>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap2/js/popper.js"></script>
<script src="vendor/bootstrap2/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
</body>
</html>
