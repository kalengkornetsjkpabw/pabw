<!doctype html>
<html>
<head>

  <meta charset="UTF-8">
  <title>Sigap</title>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajak/libs/
  jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app2.css')); ?>">

  <link href="css/bootstrap2.min.css" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="css/creative.min.css" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

</head>

<body>
  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/koorpage"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/koorpage"><font size="2">Home</font></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/login"><font size="2">Logout</font></a>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>


        <!-- slider section -->
        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Koordinator</h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
                  </div>
                  <a href="#myModal" id="go-next" class="go-next"><i class="fa fa-angle-down"></i></a>
                </div> <!-- /slider -->
              </div><!-- /col-md-12 -->
            </div><!-- /row -->
          </div><!-- container-fluid -->
        </section><!-- /slider section -->
      </div> <!-- /header-cover-section -->
</div>
<br><br>



<div class="container">
<center><h1><font size="5">Bentuk Tim</font></h1></center>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<div class="tab-content">
<div class="tab-pane fade show active" id="nav-tab-card">


</div> <!-- tab-pane.// -->


</div> <!-- tab-content .// -->





  <?php if($message = Session::get('success')): ?>
  <div class="success">
  	<p><?php echo e($message); ?></p>
  </div>
  <?php endif; ?>
  <br/>
  <!-- <a href="<?php echo e(route('user.create')); ?>" class="button2">Tambah data </a> -->
  <br/>


  <font size="4" face="Open Sans" >
    <br/><br/>
  <table class="table table-striped">
  <tr>
  	<th>Nama</th>
  	<th>Tempat Tinggal</th>
  	<th>Jenis Kelamin </th>
  	<th>Keahlian</th>
  	<th>Email </th>
    <th>Status</th>
  	<th colspan="2"></th>
  </tr>

  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <tr>
  	<td><?php echo e($row['name']); ?></td>
  	<td><?php echo e($row['tinggal']); ?></td>
  	<td><?php echo e($row['gender']); ?></td>
  	<td><?php echo e($row['keahlian']); ?></td>
  	<td><?php echo e($row['email']); ?></td>
    <td><?php echo e($row['status']); ?></td>

  	<!-- <td class="action"><a href="<?php echo e(action('UserController@edit',$row['id'])); ?>" class="btn btn-success"> Edit </a></td>
  	<td class="action">
  		<form method="post" class="delete_form" action="<?php echo e(action('UserController@destroy', $row['id'])); ?>">
        <?php echo e(csrf_field()); ?>

        <input type="hidden" name="_method" value="DELETE" />
        <button type="submit" class="btn btn-danger">Delete</button>
       </form>
  	</td> -->
  </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </table>

  </div>



<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/bootstable.js"></script>
<script>

</script>
<br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->
			</div>
			<!-- /inner-wrapper -->




		<!-- Main js -->
		<script src="js/jquery-1.11.1.min.js"></script>
		<!-- Modernizr js -->
		<script src="js/modernizr-2.8.1.min.js"></script>
		<!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo js -->
		<script src="js/jquery.countTo.js"></script>
		<!-- stellar js -->
		<script src="js/stellar.js"></script>
		<!-- YTPlayer js -->
		<script src="js/jquery.mb.YTPlayer.js"></script>
		<script src="js/sidebarEffects.js"></script>
		<script src="js/classie.js"></script>
		<!-- smoothscroll js -->
		<script src="js/smoothscroll.min.js"></script>
		<!-- viewport js -->
		<script src="js/jquery.inview.min.js"></script>
		<!-- Scripts js -->
		<script src="js/scripts.js"></script>

		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Custom scripts for this template -->
		<script src="js/creative.min.js"></script>


</body>
</html>
