<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title>Sigap</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,900,500italic' rel='stylesheet' type='text/css'>
		<!-- Normalize CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/normalize.css')); ?>" />
		<!-- font-awesome CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>" />
		<!-- flaticon Stroke CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('/css/flaticon.css')); ?>" />
		<!-- Owl Carousel CSS -->
		<link href="<?php echo e(asset('/css/owl.carousel.css')); ?>" rel="stylesheet" media="screen">
		<!-- Owl Carousel CSS -->
		<link href="<?php echo e(asset('css/owl.theme.css')); ?>" rel="stylesheet" media="screen">
		<!-- YTPlayer CSS -->
		<link href="<?php echo e(asset('css/YTPlayer.css')); ?>" rel="stylesheet" media="screen">

		<!-- Main CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>" />
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="<?php echo e(asset('/css/responsive.css')); ?>" />

		<!-- Bootstrap core CSS -->
		<link href="<?php echo e(asset('vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">

		<!-- Custom fonts for this template -->
		<link href="<?php echo e(asset('vendor/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

		<!-- Plugin CSS -->
		<link href="<?php echo e(asset('vendor/magnific-popup/magnific-popup.css')); ?>" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="<?php echo e(asset('css/creative.min.css')); ?>" rel="stylesheet">
	</head>


	<body>
		<div class="wrapper offcanvas-container" id="offcanvas-container">
			<div class="inner-wrapper offcanvas-pusher">
				<div class="header-cover-home">
					<header>
						<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
				      <div class="container" >
				        <a class="navbar-brand js-scroll-trigger" href="#offcanvas-container"><br>SIGAP</a>
				        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				          <span class="navbar-toggler-icon"></span>
				        </button>
				        <div class="collapse navbar-collapse" id="navbarResponsive">
									<div class="collapse navbar-collapse" id="navbarResponsive">
										<ul class="navbar-nav ml-auto">
											<li class="nav-item">
												<a class="nav-link js-scroll-trigger" href="/koordinator">Home</a>
											</li>
											<li class="nav-item dropdown">
													<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
														Koordinator <span class="caret"></span>
													</a>

													<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
															<a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
																 onclick="event.preventDefault();
																							 document.getElementById('logout-form').submit();">
																	<?php echo e(__('Logout')); ?>

															</a>

															<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
																	<?php echo csrf_field(); ?>
															</form>
													</div>
											</li>
				          <!--  <li class="nav-item">
				              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
				            </li>
				            <li class="nav-item">
				              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
				            </li>-->
				            <!-- <li class="nav-item">
				              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
				            </li> -->
				          </ul>
				        </div>
				      </div>
				    </nav>
					</header>


					<!-- slider section -->
					<section class="slider-section">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="slider">
										<div class="slideshow-text">
											<h1>Koordinator</h1>
											<p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
										</div>
										<a href="#services" id="go-next" class="go-next"><i class="fa fa-angle-down"></i></a>
									</div> <!-- /slider -->
								</div><!-- /col-md-12 -->
							</div><!-- /row -->
						</div><!-- container-fluid -->
					</section><!-- /slider section -->
				</div> <!-- /header-cover-section -->

				<!-- service-section -->
				<section id="services" class="service-section section-content">
					<div class="container">
						<div class="row">
							<div class="col-md-6 left-service-block">
								<div class="row">
									<div class="col-md-4 col-sm-6">
										<a  href="/bentuktim">
										<div class="service-block">
											<div class="flaticon-bull6 stroke-icon"></div>
											<div class="service-text">
												<span>Bentuk </span> Tim
											</div>
										</div> <!-- /service-block -->
										</a>
									</div> <!-- /col-md-4 -->
									<div class="col-md-4 col-sm-6">
										<a  href="/cektim">
										<div class="service-block">
											<div class="flaticon-vacations1 stroke-icon"></div>
											<div class="service-text">
												<span>Mobilisasi</span> Tim
											</div>
										</div> <!-- /service-block -->
									</div> <!-- /col-md-4 -->
									<div class="col-md-4 col-sm-6">
										<a  href="/login">
										<div  class="service-block">
											<div class="flaticon-business73 stroke-icon"></div>
											<div class="service-text">
											<span>Keluar</span>  <br> <br>
											</div>
										</div>
									</a><!-- /service-block -->
									</div> <!-- /col-md-4 -->
									 <!-- /col-md-4 -->
								</div> <!-- /row -->
							</div> <!-- /left-service-block -->
							<div class="col-md-6 right-intro-block">

								<p>Safety is a Choise not a Chance</p>
							</div>
						</div><!-- /row -->
					</div><!-- /container -->
				</section><!-- /service-section -->


				<!-- miscellaneous-section -->
				<section class="miscellaneous-section section-content" data-stellar-vertical-offset="-150" data-stellar-background-ratio="0.5">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6">
								<div class="intro-text white-bg">
									<h2>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</h2>
									<p>Sistem Manajemen Tim Kesehatan Bencana merupakan portal pengelolaan tim kesehatan krisis bencana.
			            Aplikasi ini digunakan sebagai alat pendataan tenaga kesehatan setempat,
			            menyeleksi tenaga kerja yang layak untuk dikerahkan ke lokasi bencana, menyiapkan tim kesehatan, dan  memobilisasi tim kesehatan ke lokasi krisis bencana.</p>
								</div>
							</div>


							<div class="col-md-6">
								<!-- tab-wrap -->
								<div class="tab-wrap white-bg">
									<!-- Nav tabs -->
									<!-- <ul class="nav nav-tabs" role="tablist">
										<li class="active"><a href="#agent" role="tab" data-toggle="tab">Find an Agent</a></li>
										<li><a href="#quote" role="tab" data-toggle="tab">Get a quote</a></li>
										<li><a href="#answer" role="tab" data-toggle="tab">Answer Center</a></li>
									</ul> -->
									<!-- Tab panes -->
									<div class="tab-content">
										<div class="tab-pane fade in active" id="agent">
											Distinctively visualize bricks-and-clicks e-commerce with 24/365 core competencies. Authoritatively productivate prospective methods of empowerment without scalable.
											<img class="img-responsive" src="img/agent-bg.jpg" alt="" />
										</div>

										<div class="tab-pane fade" id="quote">
											Visualize bricks-and-clicks distinctively e-commerce with 24/365 core competencies. Authoritatively productivate prospective methods of empowerment without scalable.
											<img class="img-responsive" src="img/agent-bg.jpg" alt="" />
										</div>

										<div class="tab-pane fade" id="answer">
											Prospective methods of empowerment visualize bricks-and-clicks distinctively e-commerce with 24/365 core competencies. Authoritatively productivate without.
											<img class="img-responsive" src="img/agent-bg.jpg" alt="" />
										</div>
									</div>
								</div><!-- /tab-wrap -->
							</div>
						</div><!-- /row -->



						</div><!-- /row -->
					</div><!-- /container -->
				</section><!-- /miscellaneous-section -->


				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->
			</div>
			<!-- /inner-wrapper -->




		<!-- Main js -->
		<script src="<?php echo e(asset('js/jquery-1.11.1.min.js')); ?>"></script>
		<!-- Modernizr js -->
		<script src="<?php echo e(asset('js/modernizr-2.8.1.min.js')); ?>"></script>
		<!-- Bootstrap js -->
		<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
		<!-- owl.carousel js -->
		<script src="<?php echo e(asset('js/owl.carousel.min.js')); ?>"></script>
		<!-- countTo js -->
		<script src="<?php echo e(asset('js/jquery.countTo.js')); ?>"></script>
		<!-- stellar js -->
		<script src="<?php echo e(asset('js/stellar.js')); ?>"></script>
		<!-- YTPlayer js -->
		<script src="<?php echo e(asset('js/jquery.mb.YTPlayer.js')); ?>"></script>
		<script src="<?php echo e(asset('js/sidebarEffects.js')); ?>"></script>
		<script src="<?php echo e(asset('js/classie.js')); ?>"></script>
		<!-- smoothscroll js -->
		<script src="<?php echo e(asset('js/smoothscroll.min.js')); ?>"></script>
		<!-- viewport js -->
		<script src="<?php echo e(asset('js/jquery.inview.min.js')); ?>"></script>
		<!-- Scripts js -->
		<script src="<?php echo e(asset('js/scripts.js')); ?>"></script>

		<script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>"></script>
		<script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>

		<!-- Plugin JavaScript -->
		<script src="<?php echo e(asset('vendor/jquery-easing/jquery.easing.min.js')); ?>"></script>
		<script src="<?php echo e(asset('vendor/scrollreveal/scrollreveal.min.js')); ?>"></script>
		<script src="<?php echo e(asset('vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>

		<!-- Custom scripts for this template -->
		<script src="<?php echo e(asset('js/creative.min.js')); ?>"></script>
	</body>
</html>
