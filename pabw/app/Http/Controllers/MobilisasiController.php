<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\koordinator;
use App\tim;

class MobilisasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      $tims = tim::all();
      return view('bentuktim', compact('users','koordinators','tims'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      // $koordinators = koordinator::all()->toArray();
      // $tims = tim::all()->toArray();
      // return view('user.create', compact('koordinators','tims'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate( $request, [
          'name'           => 'required',
          'email'    =>  'required',
          'password'     =>  'required',
          'tinggal'           => 'required',
           'gender'           => 'required',
          'status'           => 'required',
          'keahlian' => 'required',
          'provinsi' => 'required',
          'kabupaten' => 'required',
          'kecamatan' => 'required',
          'pos' => 'required'
      ]);
      $user = new user([
           'name'    =>  $request->get('name'),
           'email'     =>  $request->get('email'),
           'password'     =>  $request->get('password'),
           'tinggal'     =>  $request->get('tinggal'),
           'gender'     =>  $request->get('gender'),
           'status'     =>  $request->get('status'),
           'keahlian' => $request->get('keahlian'),
           'provinsi'     =>  $request->get('provinsi'),
           'kabupaten' => $request->get('kabupaten'),
           'kecamatan'     =>  $request->get('kecamatan'),
           'pos' => $request->get('pos'),
           'id_koordinator' => $request->get('id_koordinator'),
           'id_tim' => $request->get('id_tim'),
       ]);

       $user->save();
       return redirect('indexadmin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $user = user::find($id);
      return view('masuktim', compact('student', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'id_tim'           => 'required',
      ]);
      $user = user::find($id);
      $user->id_tim = $request->get('id_tim');
      $user->save();
      return redirect()->route('masuktim')->with('success', 'Data berhasil diupdate');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // $user = User::find($id);
      // $user->delete();
      // return redirect()->route('bentuktim')->with('success', 'Data Deleted');
    }
}
