<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class KoordinatorController extends Controller
{

    public function index(){
        if(!Session::get('/koordinator')){
          return redirect('/koordinator');
        }else {
          return view('koordinator.indexkoordinator');
        }
    }


    public function login(){
      return view('koordinator.login');
    }


  public function loginPost2(Request $request){
  $email = $request->email;
  $password = $request->password;
  if($email=="koordinator@gmail.com"){
    if($password=="koordinator"){
      Session::put('email',$email);
      Session::put('/koordinator',TRUE);
      return redirect('/koordinator/koorpage');
    } else if($email=="koordinator@gmail.com") {
      return redirect('/koordinator');
    }
    } else {
      return redirect('/koordinator');
    }
}
     public function logout(){
       Session::flush();
       return redirect('/koordinator');
     }
}
