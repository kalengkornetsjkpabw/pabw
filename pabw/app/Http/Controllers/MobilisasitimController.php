<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\koordinator;
use App\tim;
use Auth;


class MobilisasitimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      $tims = tim::all();

      return view('statusmobilisasi', compact('users,tims'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // $this->validate( $request, [
      //     'status'           => 'required'
      // ]);
      // $user = new user([
      //
      //      'status'     =>  $request->get('status')
      //  ]);
      //  $user->save();
      //  return redirect()->route('statusmobilisasi')->with('success', 'Data user berhasil ditambah.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // $user = User::find($id);
      // return view('statusmobilisasi', compact('user', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //   $this->validate($request, [
    //     'status'           => 'required'
    //   ]);
    //     $user = user::find($id);
    //
    //    $user->save();
    //    return redirect()->route('statusmobilisasi');
    // }
    public function mobilisasi(request $request){
      $user = Auth::user();
      $user->status = $request->status;

      $user->save();
      return redirect('/statusmobilisasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
