<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\tim;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
     public function index()
    {
        $users = user::all()->toArray();
        $tims = tim::all()->toArray();
        return view('user.index', compact('user','tim'));
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            //'tinggal' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string', 'max:255'],
            'keahlian' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255'],
            'provinsi' => ['required', 'string', 'max:255'],
            'kabupaten' => ['required', 'string', 'max:255'],
            'kecamatan' => ['required', 'string', 'max:255'],
            'pos' => ['required', 'string', 'max:255'],

        ]);
    }
    public function register(Request $request)
  {
      $this->validator($request->all())->validate();

      event(new Registered($user = $this->create($request->all())));

      // $this->guard()->login($user);

      return $this->registered($request, $user)
                      ?: redirect($this->redirectPath());
  }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
          //  'tinggal' => $data['tinggal'],
            'gender' => $data['gender'],
            'keahlian' => $data['keahlian'],
            'status' =>$data['status'],
            'provinsi' =>$data['provinsi'],
            'kabupaten' =>$data['kabupaten'],
            'kecamatan' =>$data['kecamatan'],
            'pos' =>$data['pos'],


        ]);
    }
}
