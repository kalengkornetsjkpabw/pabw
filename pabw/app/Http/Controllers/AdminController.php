<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class AdminController extends Controller
{

    public function index(){
        if(!Session::get('/admin')){
          return redirect('/admin');
        }else {
          return view('admin.indexadmin');
        }
    }


    public function login(){
      return view('admin.login');
    }


  public function loginPost(Request $request){
  $email = $request->email;
  $password = $request->password;
  if($email=="admin@gmail.com"){
    if($password=="admin"){
      Session::put('email',$email);
      Session::put('/admin',TRUE);
      return redirect('/admin/adminpage');
    } else if($email=="admin@gmail.com") {
      return redirect('/admin');
    }
    } else {
      return redirect('/admin');
    }
}
     public function logout(){
       Session::flush();
       return redirect('/admin');
     }
}
