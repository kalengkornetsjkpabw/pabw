<!doctype html>
<html>
<head>

  <meta charset="UTF-8">
  <title>Sigap</title>
  <link href="css/bootstrap2.min.css" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="css/creative.min.css" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

</head>

<body>
  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/home"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/home"><font size="2">Home</font></a>
                  </li>

                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <font size="2">  {{ Auth::user()->name }} </font> <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>


        <!-- slider section -->
        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Mobilisasi</h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
                  </div>
                </div> <!-- /slider -->
              </div><!-- /col-md-12 -->
            </div><!-- /row -->
          </div><!-- container-fluid -->
        </section><!-- /slider section -->
      </div> <!-- /header-cover-section -->
</div>
<br><br><br><br>



<div class="container">

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- sample: https://help.ecatholic.com/article/222-tables -->

  <!-- Trigger the modal with a button -->
  <br><br><br><br>
<font face="Open Sans" >
<table style="background-color:#ffffff; " class="table table-striped">
  <tr>
    <td><font size="5">Nama</font></td><td><font size="5">:</td> <td><font size=5>{{ Auth::user()->name }}</font></td>
  </tr>
<tr>
  <td><font size="5">Nama Tim</font></td><td style="width: 20px"><font size="5">:</td> <td><font size="5">{{ Auth::user()->miliktim['nama']}}</font></td>
</tr>
<tr>
  <td><font size="5">Lokasi Dituju</font></td> <td><font size="5">:</td> <td><font size="5">{{ Auth::user()->miliktim['provinsi']}},{{ Auth::user()->miliktim['kabupaten']}},{{ Auth::user()->miliktim['kecamatan']}},{{ Auth::user()->miliktim['pos']}}</font></td>
</tr>

<form method="post">
   {{csrf_field()}}
<!-- <tr>
  <td><font size="3">Status</font> </td> <td><font size="3">:</font></td>
  <td>
  <select name="status"  style="width: 100px; height: 25px;">
  <option value="Siaga">Siaga</option>
  <option value="Tersedia">Tersedia</option>
  <option value="Tidak Tersedia">Tidak Tersedia</option>
  <option value="Di Lokasi">Di Lokasi</option>
</select> </td>
</tr> -->
<tr>
  <td><font size="5">Status </font></td>
<td><font size="5">:</font></td>

<td><select input type="text" name="status" value="{{ Auth::user()->status}}"class="btn btn-info dropdown-toggle">
 <option value="tersedia">tersedia</option>
 <option value="tidak tersedia">tidak tersedia</option>
 <option value="dilokasi">dilokasi</option>
 <option disabled value="dikerahkan">dikerahkan</option>
</select></td>
<tr>

<br><br>
<br>
<tr>
  <td colspan="3"><input style="width:100px" type="submit" class="btn btn-success btn-lg " value="Simpan" /></td>
</tr>



</table>
</form>

  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/bootstable.js"></script>
<script>

</script>
<br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->
			</div>
			<!-- /inner-wrapper -->




		<!-- Main js -->
		<script src="js/jquery-1.11.1.min.js"></script>
		<!-- Modernizr js -->
		<script src="js/modernizr-2.8.1.min.js"></script>
		<!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo js -->
		<script src="js/jquery.countTo.js"></script>
		<!-- stellar js -->
		<script src="js/stellar.js"></script>
		<!-- YTPlayer js -->
		<script src="js/jquery.mb.YTPlayer.js"></script>
		<script src="js/sidebarEffects.js"></script>
		<script src="js/classie.js"></script>
		<!-- smoothscroll js -->
		<script src="js/smoothscroll.min.js"></script>
		<!-- viewport js -->
		<script src="js/jquery.inview.min.js"></script>
		<!-- Scripts js -->
		<script src="js/scripts.js"></script>

		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Custom scripts for this template -->
		<script src="js/creative.min.js"></script>


</body>
</html>
