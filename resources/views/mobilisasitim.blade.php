<!doctype html>
<html>
<head>

  <meta charset="UTF-8">
  <title>Sigap</title>
  <link href="css/bootstrap2.min.css" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="css/creative.min.css" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

</head>

<body>
  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/koordinator/koorpage"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/koordinator/koorpage">  <font size="2">Home</font></a>
                  </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <font size="2"> Koordinator</font><span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>


        <!-- slider section -->
        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Mobilisasi Tim</h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
                  </div>
                  <!-- <a href="#myModal" id="go-next" class="go-next"><i class="fa fa-angle-down"></i></a> -->
                </div> <!-- /slider -->
              </div><!-- /col-md-12 -->
            </div><!-- /row -->
          </div><!-- container-fluid -->
        </section><!-- /slider section -->
      </div> <!-- /header-cover-section -->
</div>
<br><br>



<div class="container">
<!-- <center><h1><font size="6">Mobilisasi Tim</font></h1></center> -->
<br><br>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- sample: https://help.ecatholic.com/article/222-tables -->

  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <!-- h4 class="modal-title">Modal Header</h4 -->
        </div>
        <div class="modal-body">

            <!-- card -->
            <!-- card -->
          <article class="card">
            <div class="card-body p-5">

<!-- <ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
    <i class="fa fa-credit-card"></i> TypeName Details</a></li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
    <i class="fab fa-paypal"></i>  Paypal</a></li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="pill" href="#nav-tab-bank">
    <i class="fa fa-university"></i>  Bank Transfer</a></li>
</ul> -->

<div class="tab-content">
<div class="tab-pane fade show active" id="nav-tab-card">
  <center><p class="alert alert-success">Bentuk Tim </p></center>

</div> <!-- tab-pane.// -->


</div> <!-- tab-content .// -->

</div> <!-- card-body.// -->
</article> <!-- card.// -->


</div>

</div>

</div>
</div><!-- Modal -->


</body>
</html>



<table class="table table-bordered">
  <thead>
    <tr>
      <th style="width:10px">No</th>
      <th>Tim</th>
      <th>Ketua Tim</th>
      <th>Lokasi Tujuan</th>
      <th style="width:10px">Daftar Anggota</th>
      <th style="width:10px">Kerahkan</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Jaguar</td>
      <td>Sasuke</td>
      <td>Dusun 2, Suroteleng, Selo, Boyolali Regency, Central Java</td>
      <td><button  class="subscribe btn btn-primary btn-block" type="button" style="width:50px">Cek</button></td>
      <td><button class="subscribe btn btn-primary btn-block" type="button" style="width:100px">Kerahkan</button></td>
    </tr>
    <tr>
      <td>2</td>
      <td>Kucing</td>
      <td>Nobita</td>
      <td>Dusun 2, Suroteleng, Selo, Boyolali Regency, Central Java</td>
      <td><button class="subscribe btn btn-primary btn-block" type="button" style="width:50px">Cek</button></td>
      <td><button class="subscribe btn btn-primary btn-block" type="button" style="width:100px">Kerahkan </button></td>
    </tr>
    <tr>
      <td>3</td>
      <td>Alpha</td>
      <td>Saitama</td>
      <td>Dusun 2, Suroteleng, Selo, Boyolali Regency, Central Java</td>
      <td><button class="subscribe btn btn-primary btn-block" type="button" style="width:50px">Cek</button></td>
      <td><button class="subscribe btn btn-primary btn-block" type="button"style="width:100px">Kerahkan</button></td>
    </tr>

  </tbody>
  </table>
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/bootstable.js"></script>
<script>

</script>
<br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->
			</div>
			<!-- /inner-wrapper -->




		<!-- Main js -->
		<script src="js/jquery-1.11.1.min.js"></script>
		<!-- Modernizr js -->
		<script src="js/modernizr-2.8.1.min.js"></script>
		<!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo js -->
		<script src="js/jquery.countTo.js"></script>
		<!-- stellar js -->
		<script src="js/stellar.js"></script>
		<!-- YTPlayer js -->
		<script src="js/jquery.mb.YTPlayer.js"></script>
		<script src="js/sidebarEffects.js"></script>
		<script src="js/classie.js"></script>
		<!-- smoothscroll js -->
		<script src="js/smoothscroll.min.js"></script>
		<!-- viewport js -->
		<script src="js/jquery.inview.min.js"></script>
		<!-- Scripts js -->
		<script src="js/scripts.js"></script>

		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Custom scripts for this template -->
		<script src="js/creative.min.js"></script>


</body>
</html>
