
<!doctype html>

<html>
<head>

  <meta charset="UTF-8">
  <title>Sigap</title>
  <!-- table css -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajak/libs/
  jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('/css/app2.css') }}">


  <link href="{{ asset('css/bootstrap2.min.css') }}" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="{{ asset('css/creative.min.css') }}" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href='http://www.jqueryscript.net/css/jquerysctipttop.css' rel="stylesheet" type="text/css">

</head>
<body>
  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/adminpage"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/adminpage"><font size="2">Home</font></a>
                  </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Admin <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>


                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>


        <!-- slider section -->
        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Staf Pengelola </h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
                  </div>
                  <a href="#bootstrap-css" id="go-next" class="go-next"><i class="fa fa-angle-down"></i></a>
                </div> <!-- /slider -->
              </div><!-- /col-md-12 -->
            </div><!-- /row -->
          </div><!-- container-fluid -->
        </section><!-- /slider section -->
      </div> <!-- /header-cover-section -->

</div>
<br><br>
<br><br>

<br><br>
<br><br>




<h1><b><font  face="Open Sans" size="5">Edit Data Tenaga Kesehatan</font></b></h1>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- sample: https://help.ecatholic.com/article/222-tables -->

  <!-- Trigger the modal with a button -->
<br><br>
<br><br>
<br><br>


<div class="row">
 <div class="col-md-12">

  @if(count($errors) > 0)

  <div class="alert alert-danger">
         <ul>
         @foreach($errors->all() as $error)
          <li>{{$error}}</li>
         @endforeach
         </ul>
  @endif
  </div>
  <table width="1000" height="500" style="background-color:white; border-radius: 25px;" >
  <form method="post" action="{{action('UserController@update', $id)}}">
   {{csrf_field()}}
   <input type="hidden" name="_method" value="PATCH" /></td>

   <tr>
     <td>Nama</td>
   <td>:</td>
     <td><input type="text" name="name" value="{{$user->name}}"class="form2"></td>
   </tr>
    <tr>
      <td>Tempat Tinggal</td>
    <td>:</td>
      <td ><input type="text" name="tinggal" value="{{$user->tinggal}}"class="form2"></td>
    </tr>
    <tr>
      <td>Jenis kelamin</td>
    <td>:</td>
      <td width="5%"><input  type="radio" name="gender" value="Laki-Laki" {{ $user->gender == 'Laki-Laki' ? 'checked' : '' }}> Laki-Laki </td>
      <td ><input type="radio" name="gender" value="Perempuan"  {{ $user->gender == 'Perempuan' ? 'checked' : '' }}>Perempuan</td>
    </tr>
    <tr>
      <td>Keahlian</td>
    <td>:</td>
      <td><input type="text" name="keahlian" value="{{$user->keahlian}}"class="form2"></td>
    </tr>
    <tr >
      <td>Email</td>
    <td>:</td>
      <td><input type="email" name="email" value="{{$user->email}}"class="form2"></td>
    </tr>
    <tr style="display:none;">
      <td>status</td>
    <td>:</td>
      <td><input type="text" name="status" value="{{$user->status}}"class="form2"></td>
    </tr>
    <tr style="display:none;" >
      <td>Password</td>
    <td>:</td>
      <td><input type="password" name="password" value="{{$user->password}}"class="form2"></td>
    </tr>
    <tr>
    <td colspan="3">  <a href="/user"><input type="button" class="btn btn-secondary btn-lg " value="kembali" /></a></td>
    <td colspan="3"><input style="width:100px" type="submit" class="btn btn-success btn-lg " value="Edit" /></td>
    </tr>







  </form>
  </table>
 </div>

</div>
  </div>

<br><br>
<br><br>
<br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->

			<!-- /inner-wrapper -->




		<!-- Main js -->
		<script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
		<!-- Modernizr js -->
		<script src="{{ asset('js/modernizr-2.8.1.min.js') }}"></script>
		<!-- Bootstrap js -->
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<!-- owl.carousel js -->
		<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
		<!-- countTo js -->
		<script src="{{ asset('js/jquery.countTo.js') }}"></script>
		<!-- stellar js -->
		<script src="{{ asset('js/stellar.js') }}"></script>
		<!-- YTPlayer js -->
		<script src="{{ asset('js/jquery.mb.YTPlayer.js') }}"></script>
		<script src="{{ asset('js/sidebarEffects.js') }}"></script>
		<script src="{{ asset('js/classie.js') }}"></script>
		<!-- smoothscroll js -->
		<script src="{{ asset('js/smoothscroll.min.js') }}"></script>
		<!-- viewport js -->
		<script src="{{ asset('js/jquery.inview.min.js') }}"></script>
		<!-- Scripts js -->
		<script src="{{ asset('js/scripts.js') }}"></script>

		<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

		<!-- Plugin JavaScript -->
		<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
		<script src="{{ asset('vendor/scrollreveal/scrollreveal.min.js') }}"></script>
		<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

		<!-- Custom scripts for this template -->
		<script src="{{ asset('js/creative.min.js') }}"></script>


</body>
</html>
