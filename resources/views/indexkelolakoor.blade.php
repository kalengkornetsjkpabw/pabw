<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Kelola Koordinator Kesehatan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />



    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <link rel="stylesheet" type="text/css" href="vendor/bootstrap3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font/font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap3/js/bootstrap.min.js"></script>
    <link href="vendor/bootstrap3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <meta charset="utf-8">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

    <!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,900,500italic' rel='stylesheet' type='text/css'>
		<!-- Normalize CSS -->
		<link rel="stylesheet" href="css/normalize.css" />
		<!-- font-awesome CSS -->
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<!-- flaticon Stroke CSS -->
		<link rel="stylesheet" href="css/flaticon.css" />
		<!-- Owl Carousel CSS -->
		<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
		<!-- Owl Carousel CSS -->
		<link href="css/owl.theme.css" rel="stylesheet" media="screen">
		<!-- YTPlayer CSS -->
		<link href="css/YTPlayer.css" rel="stylesheet" media="screen">

		<!-- Main CSS -->
		<link rel="stylesheet" href="css/style.css" />
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="css/responsive.css" />


  </head>

<body>


  <div class="header-cover-home">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/adminpage">SIGAP</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#go-next">Home</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/login">logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- slider section -->
  <section class="slider-section">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="slider">
            <div class="slideshow-text">
              <h1>ADMIN</h1>
              <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
            </div>
            <a href="#grid" id="go-next" class="go-next"><i class="fa fa-angle-down"></i></a>
          </div> <!-- /slider -->
        </div><!-- /col-md-12 -->
      </div><!-- /row -->
    </div><!-- container-fluid -->
  </section><!-- /slider section -->
</div> <!-- /header-cover-section -->



<div class="container">


<!-- Editable Table - START -->

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="text-center">Kelola Koordinator Kesehatan <!--<span class="fa fa-edit pull-right bigicon"></span> --></h4>

            <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <!--- Include the above in your HEAD tag ---------->

            <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <!---Include the above in your HEAD tag ---------->




            <!-- sample: https://help.ecatholic.com/article/222-tables -->
            <div class="container">

              <!-- Trigger the modal with a button -->
              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Tambah</button>

              <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                     <!-- <button type="button" class="close" data-dismiss="modal"><a href="/kelolakoor1"> ×</a></button> -->
                      <!-- h4 class="modal-title">Modal Header</h4 -->
                    </div>
                    <div class="modal-body">

                        <!-- card -->
                      <article class="card">
                        <div class="card-body p-5">

            <!-- <ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
            	<li class="nav-item">
            		<a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
            		<i class="fa fa-credit-card"></i> TypeName Details</a></li>
            	<li class="nav-item">
            		<a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
            		<i class="fab fa-paypal"></i>  Paypal</a></li>
            	<li class="nav-item">
            		<a class="nav-link" data-toggle="pill" href="#nav-tab-bank">
            		<i class="fa fa-university"></i>  Bank Transfer</a></li>
            </ul> -->

            <div class="tab-content">
            <div class="tab-pane fade show active" id="nav-tab-card">
            	<p class="alert alert-success">Tambah Akun </p>
            	<form role="form">
            	<!-- <div class="form-group">
                    <label><span class="hidden-xs">Type</span> </label>
                    <div class="form-inline">
                    		<select class="form-control" style="width:45%">
            				  <option> cek </option>
            				  <option>String</option>
            				  <option>Number</option>
            				  <option>Object</option>
            				  <option>Array</option>
            				</select>
                    </div>

            	</div>  -->
              <!-- form-group.// -->
              <div class="form-group">
            		<label for="id">ID</label>
            		<input type="number" class="form-control" name="id" placeholder="" required="">
            	</div>

            	<div class="form-group">
            		<label for="name">Nama</label>
            		<input type="text" class="form-control" name="name" placeholder="" required="">
            	</div> <!-- form-group.// -->

            	<div class="form-group">
            		<label for="tinggal">Tempat Tinggal</label>
            		<input type="text" class="form-control" name="tinggal" placeholder="" required="">
            	</div> <!-- form-group.// -->

            <a href="/kelolakoor"	<button class="subscribe btn btn-secondary btn-block" type="button"> Cancel  </button></a>
            	<button class="subscribe btn btn-primary btn-block" type="button"> Confirm  </button>
            	</form>

            </div> <!-- tab-pane.// -->

      
            </div> <!-- tab-content .// -->

            </div> <!-- card-body.// -->
            </article> <!-- card.// -->


            </div>

            </div>

            </div>
            </div><!-- Modal -->

            </div>



        </div>
        <div class="panel-body text-center">
            <div id="grid"></div>
        </div>
    </div>
</div>


<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/creative.min.js"></script>


<!-- Main js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- Modernizr js -->
<script src="js/modernizr-2.8.1.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- owl.carousel js -->
<script src="js/owl.carousel.min.js"></script>
<!-- countTo js -->
<script src="js/jquery.countTo.js"></script>
<!-- stellar js -->
<script src="js/stellar.js"></script>
<!-- YTPlayer js -->
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/sidebarEffects.js"></script>
<script src="js/classie.js"></script>
<!-- smoothscroll js -->
<script src="js/smoothscroll.min.js"></script>
<!-- viewport js -->
<script src="js/jquery.inview.min.js"></script>
<!-- Scripts js -->
<script src="js/scripts.js"></script>

<!-- you need to include the shieldui css and js assets in order for the grids to work -->
<link rel="stylesheet" type="text/css" href="http://www.prepbootstrap.com/Content/shieldui-lite/dist/css/light/all.min.css" />
<script type="text/javascript" src="http://www.prepbootstrap.com/Content/shieldui-lite/dist/js/shieldui-lite-all.min.js"></script>

<script type="text/javascript" src="http://www.prepbootstrap.com/Content/data/shortGridData.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#grid").shieldGrid({
            // dataSource: {
            //     data: gridData,
            //     schema: {
            //         fields: {
            //             id: { path: "id", type: Number },
            //             age: { path: "age", type: Number },
            //             name: { path: "name", type: String },
            //             tempattinggal: { path: "", type: String },
            //             month: { path: "month", type: Date },
            //             isActive: { path: "isActive", type: Boolean },
            //             email: { path: "email", type: String },
            //             transport: { path: "transport", type: String }
            //         }
            //     }
            // },
            // sorting: {
            //     multiple: true
            // },
            // rowHover: false,
            // columns: [
            //     { field: "id", title: "ID", width: "120px" },
            //     { field: "name", title: "Nama", width: "120px" },
            //     { field: "tempattinggal", title: "Tempat Tinggal" },
            //     // { field: "month", title: "Date of Birth", format: "{0:MM/dd/yyyy}", width: "120px" },
            //     // { field: "isActive", title: "Active" },
            //     // { field: "email", title: "Email Address", width: "250px" },
            //     // { field: "transport", title: "Custom Editor", width: "120px" },
            //     {
            //         width: 150,
            //         title: "Update/Delete Column",
            //         buttons: [
            //             { commandName: "edit", caption: "Edit" },
            //             { commandName: "delete", caption: "Delete" }
            //         ]
            //     }
            // ],
            editing: {
                enabled: true,
                mode: "popup",
                confirmation: {
                    "delete": {
                        enabled: true,
                        template: function (item) {
                            return "Delete row with ID = " + item.id
                        }
                    }
                }
            }
        });


    });
</script>

<style type="text/css">
    .sui-button-cell
    {
        text-align: center;
    }

    .sui-checkbox
    {
        font-size: 17px !important;
        padding-bottom: 4px !important;
    }

    .deleteButton img
    {
        margin-right: 3px;
        vertical-align: bottom;
    }

    .bigicon
    {
        color: #5CB85C;
        font-size: 20px;
    }
</style>

<!-- Editable Table - END -->

</div>


				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->

</body>
</html>
