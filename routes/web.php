<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cek', function () {
    return view('lintang');
});
// Route::get('/login2', function () {
//     return view('login2');
// });
Route::get('/firstpage', function () {
    return view('index');
});
Route::get('/', function () {
    return view('welcome');
});

// Route::get('/adminpage', function () {
//     return view('indexadmin');
// });

Route::get('/kelolakoor2', function () {
    return view('indexkelolakoor3');
});

// Route::get('/koorpage', function () {
//     return view('indexkoordinator');
// });
// Route::get('/bentuktim', function () {
//     return view('bentuktim');
// });
Route::get('/mobilisasitim', function () {
    return view('mobilisasitim');
});
// Route::get('/tenagapage', function () {
//     return view('indextenaga');
// });
// Route::get('/lihattim', function () {
//     return view('lihattim');
// });
Route::get('/login2', function () {
    return view('admin.login');
});
Route::get('/login3', function () {
    return view('koordinator.login');
});
// Route::get('/mobilisasi', function () {
//     return view('statusmobilisasi');
// });
// Route::get('/kelolatenaga2', function () {
//     return view('kelolatenaga2');
// });
Route::post('statusmobilisasi','MobilisasitimController@mobilisasi');
Route::get('lihattim','AnggotaController@index');
Route::resource('masuktim','BentukController');
Route::resource('buattim','TimController');
Route::post('cektim','LihattimController@status');
Route::get('cektim','LihattimController@index');
Route::resource('bentuktim','MobilisasiController');

Route::get('statusmobilisasi','MobilisasitimController@index');
Route::post('bentuktim','UserController@inputtim');
Route::resource('user','UserController');
// Route::resource('koordinator','KoordinatorController');
Route::resource('tim','TimController');

//login admin
// Route::resource('/adminpage','AdminController@index');
Route::get('/admin/adminpage','AdminController@index');
Route::get('/admin','AdminController@login');
Route::post('/loginPost','AdminController@loginPost');
Route::get('/logout','AdminController@logout');


//login koordinator
Route::get('/koordinator/koorpage','KoordinatorController@index');
Route::get('/koordinator','KoordinatorController@login');
Route::post('/loginPost2','KoordinatorController@loginPost2');
Route::get('/logout','KoordinatorController@logout');



Auth::routes();
Route::get('home', 'HomeController@index')->name('home');
