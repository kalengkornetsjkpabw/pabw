<!doctype html>
<html>
<head>

  <meta charset="UTF-8">
  <title>Sigap</title>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajak/libs/
  jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app2.css')); ?>">

  <link href="css/bootstrap2.min.css" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="css/creative.min.css" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

</head>

<body>
  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/koordinator/koorpage"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/koordinator/koorpage"><font size="2">Home</font></a>
                  </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      <font size="2">Koordinator</font>

                       </font>  <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                           <?php echo e(__('Logout')); ?>

                          </a>

                          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                              <?php echo csrf_field(); ?>
                          </form>
                      </div>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>


        <!-- slider section -->
        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Bentuk Tim</h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
              </div><!-- /col-md-12 -->
            </div><!-- /row -->
          </div><!-- container-fluid -->
        </section><!-- /slider section -->
      </div> <!-- /header-cover-section -->
</div>




<div class="container">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- sample: https://help.ecatholic.com/article/222-tables -->

  <!-- Trigger the modal with a button -->


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <!-- h4 class="modal-title">Modal Header</h4 -->
        </div>
        <div class="modal-body">

            <!-- card -->
            <!-- card -->
          <article class="card">
            <div class="card-body p-5">

<!-- <ul class="nav bg-light nav-pills rounded nav-fill mb-3" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="pill" href="#nav-tab-card">
    <i class="fa fa-credit-card"></i> TypeName Details</a></li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">
    <i class="fab fa-paypal"></i>  Paypal</a></li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="pill" href="#nav-tab-bank">
    <i class="fa fa-university"></i>  Bank Transfer</a></li>
</ul> -->

<div class="tab-content">
<div class="tab-pane fade show active" id="nav-tab-card">
  <center><p class="alert alert-success">Bentuk Tim </p></center>
  <form role="form">
  <!-- <div class="form-group">
        <label><span class="hidden-xs">Type</span> </label>
        <div class="form-inline">
            <select class="form-control" style="width:45%">
          <option> cek </option>
          <option>String</option>
          <option>Number</option>
          <option>Object</option>
          <option>Array</option>
        </select>
        </div>

  </div>  -->
  <!-- form-group.// -->



  <div class="form-group">
    <label for="tim">Nama Tim</label>
    <input type="text" class="form-control" name="id" placeholder="" required="">
  </div>

  <div class="form-group">
    <label for="name">Ketua Tim</label>
    <input type="text" class="form-control" name="name" placeholder="" required="">
  </div> <!-- form-group.// -->

  <div class="form-group">
    <label for="name">Lokasi Tujuan</label>
    <input type="text" class="form-control" name="name" placeholder="" required="">
  </div> <!-- form-group.// -->

  <div class="form-group">
    <label for="tinggal">Anggota Tim</label>
    <input type="text" class="form-control" name="tinggal" placeholder="anggota 1" required="">
    <br>
    <input type="text" class="form-control" name="tinggal" placeholder="anggota 2" required="">
    <br>
    <input type="text" class="form-control" name="tinggal" placeholder="anggota 3" required="">
    <br>
    <input type="text" class="form-control" name="tinggal" placeholder="anggota 4" required="">
    <br>
    <input type="text" class="form-control" name="tinggal" placeholder="anggota 5" required="">
  </div> <!-- form-group.// -->

<a href="/bentuktim"	<button class="subscribe btn btn-secondary btn-block" type="button"> Cancel  </button></a>
  <button class="subscribe btn btn-primary btn-block" type="button"> Confirm  </button>
  </form>

</div> <!-- tab-pane.// -->


</div> <!-- tab-content .// -->

</div> <!-- card-body.// -->
</article> <!-- card.// -->


</div>

</div>

</div>
</div><!-- Modal -->

<!-- <table class="table table-bordered">
  <thead>
    <tr>
      <th>No</th>
      <th>ID</th>
      <th>Nama</th>
      <th>Tempat Tinggal</th>
      <th>Jenis Kelamin</th>
      <th>Keahlian</th>
      <th>Email</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>4444</td>
      <td>Naruto</td>
      <td>Solo</td>
      <td>Laki-laki</td>
      <td>Dokter bedah</td>
      <td>naruto@gmail.com</td>
        <td>Siaga</td>
    </tr>
  </tbody>
  </table> -->


  <?php if($message = Session::get('success')): ?>
  <div class="success">
  	<p><?php echo e($message); ?></p>
  </div>
  <?php endif; ?>
  <br/>
  <!-- <a href="<?php echo e(route('user.create')); ?>" class="button2">Tambah data </a> -->
  <br/>
  <br/>
  <br/>
  <!-- <a href="/register">  <button type="button" class="button2" style="background-color: #333399; " >Tambah</button></a> -->
  <!-- <table>
    <tr>
      <td><button type="button" class="subscribe btn btn-secondary btn-block" data-toggle="modal" data-target="#myModal">Buat Tim</button></td>
    <a href="/cek"> <td><button type="button" class="btn btn-info btn-lg" >Lihat Tim</button></td></a>
    </tr>
  </table> -->
  <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#buat">
  Buat Tim
</button> -->
<!-- Modal -->
<div class="modal fade" id="buat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <h5 class="modal-title" id="exampleModalLongTitle"><center> Buat tim dan lokasi tujuan</center></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo e(url('buattim')); ?>">
          <?php echo e(csrf_field()); ?>

          <table style="background-color:white;">
            <tr>
              <td colspan="3" class="row_alert"><?php if(count($errors) > 0): ?>
        <div class="error">
         <ul>
         <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <li><?php echo e($error); ?></li>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </ul>
        </div>
        <?php endif; ?>
        <?php if(\Session::has('success')): ?>
        <div class="alert">
         <p><?php echo e(\Session::get('success')); ?></p>
        </div>
        <?php endif; ?>
        <br/>
      </td>
      </tr>

          <div class="form-group row">
            <label for="provinsi" class="col-md-4 col-form-label text-md-right"> Nama Tim </label>
              <div class="col-lg-8">
            <input type="text" name="nama" class="form2">
          </div>
        </div>
              <div class="form-group row">
                <label for="provinsi" class="col-md-4 col-form-label text-md-right">Provinsi</label>
                <div class="col-lg-8">
                <input list="provinsi" name="provinsi" class="form2">
                <datalist id="provinsi" >
                        <?php

                        $provinsis=DB::table('provinces')->get();

                        ?>

                        <?php $__currentLoopData = $provinsis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $provinsi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <option value="<?php echo e($provinsi->name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </datalist>
                </div>
              </div>
              <div class="form-group row">
                <label for="kabupaten" class="col-md-4 col-form-label text-md-right">Kabupaten/Kota</label>
                <div class="col-lg-8">
                <input list="kabupaten" name="kabupaten" class="form2">
                <datalist id="kabupaten" >
                        <?php

                        $regencies=DB::table('regencies')->get();

                        ?>

                        <?php $__currentLoopData = $regencies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kabupaten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <option value="<?php echo e($kabupaten->name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </datalist>
                </div>
              </div>
              <div class="form-group row">
                <label for="kecamatan" class="col-md-4 col-form-label text-md-right">Kecamatan</label>
                <div class="col-lg-8">
                <input list="kecamatan" name="kecamatan" class="form2">
                <datalist id="kecamatan" >
                        <?php

                        $villages=DB::table('villages')->get();

                        ?>

                        <?php $__currentLoopData = $villages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kecamatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <option value="<?php echo e($kecamatan->name); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </datalist>
                </div>
              </div>
              <div class="form-group row">
                <label for="pos" class="col-md-4 col-form-label text-md-right"> Kode Pos </label>
                  <div class="col-lg-8">
                <input type="text" name="pos" class="form2">
              </div>
            </div>



      </table>
         <!-- <input type="submit" class="button" value="Buat"> -->

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <font size="4" face="Open Sans" >
    <br/><br/>
  <table class="table table-striped" >
    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#buat"> Buat Tim</button>
    <!-- <a href="/buattim">  <button type="button" class="btn btn-success btn-lg" >Buat Tim</button></a> -->
      <!-- <a href="/cektim">  <button type="button" class="btn btn-secondary btn-lg" >Lihat Tim</button></a> -->
    <br><br>
  <tr>
  	<th width="15%">Nama</th>
  	<th>Tempat Tinggal</th>
  	<th>Jenis Kelamin </th>
  	<th>Keahlian</th>
  	<th>Email </th>
    <th>Status</th>
  	<th>Tim</th>
    <th colspan="1"></th>
  </tr>

  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <tr>
  	<td><?php echo e($row['name']); ?></td>
  	<td><?php echo e($row['provinsi']); ?>,<?php echo e($row['kabupaten']); ?>,<?php echo e($row['kecamatan']); ?>,
    <?php echo e($row['pos']); ?></td>
  	<td><?php echo e($row['gender']); ?></td>
  	<td><?php echo e($row['keahlian']); ?></td>
  	<td><?php echo e($row['email']); ?></td>
    <td><?php echo e($row['status']); ?></td>

    <?php if($row['id_tim']==""): ?>
      	<td class="action"><a href="<?php echo e(action('TimController@edit',$row['id'])); ?>" class="btn btn-outline-success" data-toggle="modal" data-target="#tim<?php echo e($row['id']); ?>"> Pilih Tim </a></td>
    <?php else: ?>
      <td> <?php echo e($row->miliktim['nama']); ?><td>
    <?php endif; ?>

  </tr>
  <!-- Modal -->
<div class="modal fade" id="tim<?php echo e($row['id']); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <div class="modal-header">
      <center>  <h5 class="modal-title" id="exampleModalLongTitle">Pilih Tim</h5></center>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" >
          <?php echo csrf_field(); ?>
          <input type="hidden" name="idrow" value="<?php echo e($row['id']); ?>"></input>
          <select name="id_tim">
            <?php $__currentLoopData = $tims; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($row['id']); ?>"><?php echo e($row['nama']); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <button type="submit" class="btn btn-primary">Pilih</button>

      </form>
      </div>
    </div>
    </div>
  </div>
</div>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </table>

  </div>



<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/bootstable.js"></script>
<script>

</script>
<br><br><br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->
			</div>
			<!-- /inner-wrapper -->




		<!-- Main js -->
		<script src="js/jquery-1.11.1.min.js"></script>
		<!-- Modernizr js -->
		<script src="js/modernizr-2.8.1.min.js"></script>
		<!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo js -->
		<script src="js/jquery.countTo.js"></script>
		<!-- stellar js -->
		<script src="js/stellar.js"></script>
		<!-- YTPlayer js -->
		<script src="js/jquery.mb.YTPlayer.js"></script>
		<script src="js/sidebarEffects.js"></script>
		<script src="js/classie.js"></script>
		<!-- smoothscroll js -->
		<script src="js/smoothscroll.min.js"></script>
		<!-- viewport js -->
		<script src="js/jquery.inview.min.js"></script>
		<!-- Scripts js -->
		<script src="js/scripts.js"></script>

		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Custom scripts for this template -->
		<script src="js/creative.min.js"></script>


</body>
</html>
