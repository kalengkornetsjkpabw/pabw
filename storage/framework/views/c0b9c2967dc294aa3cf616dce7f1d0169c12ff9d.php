
<!doctype html>

<html>
<head>

  <meta charset="UTF-8">
  <title>Sigap</title>
  <!-- table css -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajak/libs/
  jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('/css/app2.css')); ?>">

  </script>
  <link href="css/bootstrap2.min.css" rel="stylesheet" type="text/css">

  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="css/creative.min.css" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

</head>

<body>
  <div class="wrapper offcanvas-container" id="offcanvas-container">
    <div class="inner-wrapper offcanvas-pusher">
      <div class="header-cover-home">
        <header>
          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
              <a class="navbar-brand js-scroll-trigger" href="/admin/adminpage"><br><br><font size="3">SIGAP</font></a>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/admin/adminpage"><font size="2">Home</font></a>
                  </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <font size="2" >   Staf Pengelola</font>
                       <span class="caret"></span>
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              <?php echo e(__('Logout')); ?>

                          </a>

                          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                              <?php echo csrf_field(); ?>
                          </form>
                      </div>
                  </li>
                <!--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                  </li>-->
                  <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </nav>
        </header>


        <!-- slider section -->
        <section class="slider-section">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="slider">
                  <div class="slideshow-text">
                    <h1>Kelola Tenaga Kesehatan</h1>
                    <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
                  </div>
                </div> <!-- /slider -->
              </div><!-- /col-md-12 -->
            </div><!-- /row -->
          </div><!-- container-fluid -->
        </section><!-- /slider section -->
      </div> <!-- /header-cover-section -->
</div>
<br><br>



<div class="container">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- sample: https://help.ecatholic.com/article/222-tables -->

  <!-- Trigger the modal with a button -->



  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <!-- h4 class="modal-title">Modal Header</h4 -->
        </div>


          <article class="card">
            <div class="card-body p-5">


<div class="tab-content">
<div class="tab-pane fade show active" id="nav-tab-card">
<center>  <p class="alert alert-success"><font size="2px">Tambah Akun</font> </p></center>
  <form role="form" autocomplete="off">


  <div class="container">
  <div class="form-group row">
    <label for="name"><?php echo e(__('Nama')); ?></label>

        <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" value="<?php echo e(old('name')); ?>" required autofocus>

        <?php if($errors->has('name')): ?>
            <span class="invalid-feedback" role="alert">
                <strong><?php echo e($errors->first('name')); ?></strong>
            </span>
        <?php endif; ?>

  </div> <!-- form-group.// -->
  <div class="form-group row">

      <label for="email"><?php echo e(__('Alamat Email')); ?></label>
          <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required>

          <?php if($errors->has('email')): ?>
              <span class="invalid-feedback" role="alert">
                  <strong><?php echo e($errors->first('email')); ?></strong>
              </span>
          <?php endif; ?>

      </div>
          <div class="form-group row">
              <label for="tinggal"><?php echo e(__('Tempat Tinggal')); ?></label>

                  <input id="tinggal" type="text" class="form-control<?php echo e($errors->has('tinggal') ? ' is-invalid' : ''); ?>" name="tinggal" value="<?php echo e(old('tinggal')); ?>" required>

                  <?php if($errors->has('tinggal')): ?>
                      <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($errors->first('tinggal')); ?></strong>
                      </span>
                  <?php endif; ?>
          </div>

          <div class="" style="background-color:white;">
            <table style="background-color:white;">
            <tr><td>  <label for="gender"><?php echo e(__('Jenis Kelamin')); ?></label></tr></td>

                  <!-- <input id="gender" type="text" class="form-control<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>" name="gender" value="<?php echo e(old('gender')); ?>" required> -->
                <tr>
                <td style="width:10px">Laki-Laki</td><td> <input id="gender"  type="radio" class="form-control<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>" name="gender" value="Laki-Laki"></td>
                <td>Perempuan</td><td><input id="gender"  type="radio" class="form-control<?php echo e($errors->has('gender') ? ' is-invalid' : ''); ?>" name="gender" value="Perempuan"></td>
               </tr>
                 </table>
                  <?php if($errors->has('gender')): ?>
                      <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($errors->first('gender')); ?></strong>
                      </span>
                  <?php endif; ?>
          </div>

          <div class="form-group row">
              <label for="keahlian" ><?php echo e(__('Keahlian')); ?></label>

                  <input id="keahlian" type="text" class="form-control<?php echo e($errors->has('keahlian') ? ' is-invalid' : ''); ?>" name="keahlian" value="<?php echo e(old('keahlian')); ?>" required>

                  <?php if($errors->has('keahlian')): ?>
                      <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($errors->first('keahlian')); ?></strong>
                      </span>
                  <?php endif; ?>

          </div>

          <div class="form-group row">
              <label for="status" ><?php echo e(__('Status')); ?></label>

                  <input id="status" type="text" class="form-control<?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>" name="status" value="tersedia" required>

                  <?php if($errors->has('status')): ?>
                      <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($errors->first('status')); ?></strong>
                      </span>
                  <?php endif; ?>

          </div>

          <div class="form-group row">
              <label for="password"><?php echo e(__('Password')); ?></label>

                  <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                  <?php if($errors->has('password')): ?>
                      <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($errors->first('password')); ?></strong>
                      </span>
                  <?php endif; ?>

          </div>

          <div class="form-group row">
              <label for="password-confirm"><?php echo e(__('Confirm Password')); ?></label>

                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

          </div>



<a href="/user"	<button class="subscribe btn btn-secondary btn-block" type="button"> Cancel  </button></a>

  <button class="subscribe btn btn-secondary btn-block" style="background-color:#00802b;" type="submit"> <?php echo e(__('Register')); ?>  </button>
  </form>

</div> <!-- tab-pane.// -->

</div>
</div> <!-- tab-content .// -->

</div> <!-- card-body.// -->
</article> <!-- card.// -->


</div>

</div>

</div>
</div><!-- Modal -->

<?php if($message = Session::get('success')): ?>
<div class="success">
	<p><?php echo e($message); ?></p>
</div>
<?php endif; ?>
<br/>
<!-- <a href="<?php echo e(route('user.create')); ?>" class="button2">Tambah data </a> -->
<br/>
<br/>
<br/>
<a href="/register">  <button type="button" class="button2" style="background-color: #333399; " >Tambah</button></a>
<font size="4" face="Open Sans" >
  <br/><br/>
<table class="table table-striped" style="width: 95%;">
<tr>
	<th>Nama</th>
	<th>Jenis Kelamin </th>
	<th>Keahlian</th>
	<th>Email </th>
	<th colspan="2"></th>
</tr>

<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
	<td><?php echo e($row['name']); ?></td>
	<td><?php echo e($row['gender']); ?></td>
	<td><?php echo e($row['keahlian']); ?></td>
	<td><?php echo e($row['email']); ?></td>

	<td class="action"><a href="<?php echo e(action('UserController@edit',$row['id'])); ?>" class="btn btn-success"> Edit </a></td>
	<td class="action">
		<form method="post" class="delete_form" action="<?php echo e(action('UserController@destroy', $row['id'])); ?>">
      <?php echo e(csrf_field()); ?>

      <input type="hidden" name="_method" value="DELETE" />
      <button type="submit" class="btn btn-danger">Delete</button>
     </form>
	</td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>
</div>


<script>
  $(document).ready(function(){
   $('.delete_form').on('submit', function(){
    if(confirm("Apakah kamu yakin akan menghapus data?"))
    {
     return true;
    }
    else
    {
     return false;
    }
   });
  });
</script>

<br><br><br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



				<!--Video Section Ends Here-->

			<!-- /inner-wrapper -->

		<!-- Main js -->
		<script src="js/jquery-1.11.1.min.js"></script>
		<!-- Modernizr js -->
		<script src="js/modernizr-2.8.1.min.js"></script>
		<!-- Bootstrap js -->
		<script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel js -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo js -->
		<script src="js/jquery.countTo.js"></script>
		<!-- stellar js -->
		<script src="js/stellar.js"></script>
		<!-- YTPlayer js -->
		<script src="js/jquery.mb.YTPlayer.js"></script>
		<script src="js/sidebarEffects.js"></script>
		<script src="js/classie.js"></script>
		<!-- smoothscroll js -->
		<script src="js/smoothscroll.min.js"></script>
		<!-- viewport js -->
		<script src="js/jquery.inview.min.js"></script>
		<!-- Scripts js -->
		<script src="js/scripts.js"></script>

		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Custom scripts for this template -->
		<script src="js/creative.min.js"></script>


</body>
</html>
