<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Kelola Koordinator Kesehatan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />



    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <link rel="stylesheet" type="text/css" href="vendor/bootstrap3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font/font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap3/js/bootstrap.min.js"></script>
    <link href="vendor/bootstrap3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <meta charset="utf-8">
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

    <!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,900,500italic' rel='stylesheet' type='text/css'>
		<!-- Normalize CSS -->
		<link rel="stylesheet" href="css/normalize.css" />
		<!-- font-awesome CSS -->
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<!-- flaticon Stroke CSS -->
		<link rel="stylesheet" href="css/flaticon.css" />
		<!-- Owl Carousel CSS -->
		<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
		<!-- Owl Carousel CSS -->
		<link href="css/owl.theme.css" rel="stylesheet" media="screen">
		<!-- YTPlayer CSS -->
		<link href="css/YTPlayer.css" rel="stylesheet" media="screen">

		<!-- Main CSS -->
		<link rel="stylesheet" href="css/style.css" />
		<!-- Responsive CSS -->
		<link rel="stylesheet" href="css/responsive.css" />


  </head>

<body>


  <div class="header-cover-home">
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/adminpage">SIGAP</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#go-next">Home</a>
          </li>

          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="/login">logout</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- slider section -->
  <section class="slider-section">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="slider">
            <div class="slideshow-text">
              <h1>ADMIN</h1>
              <p>SISTEM MANAJEMEN TIM KESEHATAN BENCANA</p>
            </div>
            <a href="#table" id="go-next" class="go-next"><i class="fa fa-angle-down"></i></a>
          </div> <!-- /slider -->
        </div><!-- /col-md-12 -->
      </div><!-- /row -->
    </div><!-- container-fluid -->
  </section><!-- /slider section -->
</div> <!-- /header-cover-section -->



<div class="container">


<!-- Editable Table - START -->

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/creative.min.js"></script>


<!-- Main js -->
<script src="js/jquery-1.11.1.min.js"></script>
<!-- Modernizr js -->
<script src="js/modernizr-2.8.1.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- owl.carousel js -->
<script src="js/owl.carousel.min.js"></script>
<!-- countTo js -->
<script src="js/jquery.countTo.js"></script>
<!-- stellar js -->
<script src="js/stellar.js"></script>
<!-- YTPlayer js -->
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/sidebarEffects.js"></script>
<script src="js/classie.js"></script>
<!-- smoothscroll js -->
<script src="js/smoothscroll.min.js"></script>
<!-- viewport js -->
<script src="js/jquery.inview.min.js"></script>
<!-- Scripts js -->
<script src="js/scripts.js"></script>

<!-- you need to include the shieldui css and js assets in order for the grids to work -->
<link rel="stylesheet" type="text/css" href="http://www.prepbootstrap.com/Content/shieldui-lite/dist/css/light/all.min.css" />
<script type="text/javascript" src="http://www.prepbootstrap.com/Content/shieldui-lite/dist/js/shieldui-lite-all.min.js"></script>

<script type="text/javascript" src="http://www.prepbootstrap.com/Content/data/shortGridData.js"></script>
<br><br><br>
<div class="container">
  <h1>Kelola Koordinator Kesehatan</h1>
<form >
  <div id="table" class="table-editable">
    <span class="table-add glyphicon glyphicon-plus"></span>
    <table class="table">
      <tr>
        <th>ID</th>
        <th>Nama</th>
        <th>Tempat Tinggal</th>
        <th></th>
        <th></th>
      </tr>
      <tr>
        <td contenteditable="true">1111</td>
        <td contenteditable="true">anonim</td>
        <td contenteditable="true">Yogyakarta</td>
        <td>
          <span class="table-remove glyphicon glyphicon-remove"></span>
        </td>
        <td>
          <span class="table-up glyphicon glyphicon-arrow-up"></span>
          <span class="table-down glyphicon glyphicon-arrow-down"></span>
        </td>
      </tr>
      <tr>
        <td contenteditable="true">1111</td>
        <td contenteditable="true">anonim</td>
        <td contenteditable="true">Yogyakarta</td>
        <td>
          <span class="table-remove glyphicon glyphicon-remove"></span>
        </td>
        <td>
          <span class="table-up glyphicon glyphicon-arrow-up"></span>
          <span class="table-down glyphicon glyphicon-arrow-down"></span>
        </td>
      </tr>
      <tr>
        <td contenteditable="true">1111</td>
        <td contenteditable="true">anonim</td>
        <td contenteditable="true">Yogyakarta</td>
        <td>
          <span class="table-remove glyphicon glyphicon-remove"></span>
        </td>
        <td>
          <span class="table-up glyphicon glyphicon-arrow-up"></span>
          <span class="table-down glyphicon glyphicon-arrow-down"></span>
        </td>
      </tr>
      <tr>
        <td contenteditable="true">1111</td>
        <td contenteditable="true">anonim</td>
        <td contenteditable="true">Yogyakarta</td>
        <td>
          <span class="table-remove glyphicon glyphicon-remove"></span>
        </td>
        <td>
          <span class="table-up glyphicon glyphicon-arrow-up"></span>
          <span class="table-down glyphicon glyphicon-arrow-down"></span>
        </td>
      </tr>


      <!-- This is our clonable table line -->
      <tr class="hide">
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td contenteditable="true"></td>
        <td>
          <span class="table-remove glyphicon glyphicon-remove"></span>
        </td>
        <td>
          <span class="table-up glyphicon glyphicon-arrow-up"></span>
          <span class="table-down glyphicon glyphicon-arrow-down"></span>
        </td>
      </tr>
    </table>
  </div>

  <button id="export-btn" class="btn btn-primary" >Simpan Data</button>
  <p id="export"></p>
</div>
</form>
<style type="text/css">
    .sui-button-cell
    {
        text-align: center;
    }

    .sui-checkbox
    {
        font-size: 17px !important;
        padding-bottom: 4px !important;
    }

    .deleteButton img
    {
        margin-right: 3px;
        vertical-align: bottom;
    }

    .bigicon
    {
        color: #5CB85C;
        font-size: 20px;
    }
</style>


<!-- Editable Table - END -->

</div>
<br><br>

				<!-- footer-section -->
				<footer class="footer-section">
					<!-- container -->
					<div class="container-fluid">
						<!-- row -->
						<div class="row">
							<div class="col-md-6">
								<div class="copyright-text">
									Copyright &copy; 2018. All Rights Reserved. By KalengKornet					</div><!--  /copyright-text -->
							</div>

						</div><!-- /row -->
					</div><!-- /container -->
				</footer><!-- /footer-section -->



</body>
</html>

<script type="text/javascript">
var $TABLE = $('#table');
var $BTN = $('#export-btn');
var $EXPORT = $('#export');

$('.table-add').click(function () {
var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
$TABLE.find('table').append($clone);
});

$('.table-remove').click(function () {
$(this).parents('tr').detach();
});

$('.table-up').click(function () {
var $row = $(this).parents('tr');
if ($row.index() === 1) return; // Don't go above the header
$row.prev().before($row.get(0));
});

$('.table-down').click(function () {
var $row = $(this).parents('tr');
$row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.click(function () {
var $rows = $TABLE.find('tr:not(:hidden)');
var headers = [];
var data = [];

// Get the headers (add special header logic here)
$($rows.shift()).find('th:not(:empty)').each(function () {
  headers.push($(this).text().toLowerCase());
});

// Turn all existing rows into a loopable array
$rows.each(function () {
  var $td = $(this).find('td');
  var h = {};

  // Use the headers from earlier to name our hash keys
  headers.forEach(function (header, i) {
    h[header] = $td.eq(i).text();
  });

  data.push(h);
});

// Output the result
// $EXPORT.text(JSON.stringify(data));
});
</script>
